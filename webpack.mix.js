const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').
    sass('resources/sass/app.scss', 'public/css').
    sass('resources/sass/login.scss', 'public/css').
    extract().
    version();

// plugins scripts for ticket page with custom script
mix.combine([
    'resources/vendor/DataTables/datatables.js',
    'resources/vendor/jasny/jasny-bootstrap.min.js',
    'resources/vendor/PapaParse/papaparse.js',// csv parser
    'resources/js/ticket.js',
], 'public/js/ticket.min.js').version();

// plugins styles for ticket page with custom style
mix.combine([
    'resources/vendor/DataTables/datatables.css',
    'resources/vendor/jasny/jasny-bootstrap.min.css',
], 'public/css/ticket.min.css').version();
