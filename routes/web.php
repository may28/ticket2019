<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/ie', function(){
    return view('ie');
})->name('ie');
Auth::routes();
Route::get('/my-tickets', 'HomeController@index')->name('my-tickets');

// api routes
Route::prefix('api')->middleware('auth', 'throttle:30,1')->group(function () {
    Route::get('/user/tickets', 'TicketController@getUserTickets');
    Route::post('/user/ticket', 'TicketController@store')->name('store-ticket');
    Route::post('/csv/tickets', 'TicketController@storeCsv')->name('csv-tickets');
});
