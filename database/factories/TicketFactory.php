<?php

use App\Model\Ticket;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Ticket::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'event_name' => $faker->sentence,
        'event_date' => $faker->dateTimeBetween('-6 months', '+6 months'),
        'section' => Str::random(10),
        'row'=> $faker->numberBetween(1, 50),
        'seat' => $faker->numberBetween(1, 50),
        'status' => $faker->numberBetween(1, 4)
    ];
});
