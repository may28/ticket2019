<?php
/**
 * Created by PhpStorm.
 * User: ashucn
 * Date: 2019-04-02
 * Time: 19:32
 */

namespace App\Classes;


use App\Model\Ticket;

/**
 * Class TicketApi
 * @package App\Classes
 */
class TicketApi
{
    /**
     * @param $uid
     * @return mixed
     */
    public static function getTickets($uid, $data)
    {
        if (isset($data['keyword']) && isset($data['type'])) {
            if ($data['type'] == "1") {
                // type 1: event name
                return Ticket::where([['user_id', $uid], ['event_name', 'like', "%" . $data['keyword'] . "%"]])->get();
            } elseif ($data['type'] == "2") {
                // type 2:search session/row/seat
                return Ticket::where('user_id', $uid)
                    ->where(function ($query) use ($data) {
                        $query->where('section', 'like', "%" . $data['keyword'] . "%")
                            ->orWhere('row', $data['keyword'])
                            ->orWhere('seat', $data['keyword']);
                    })->get();

            }
        }

        return Ticket::where('user_id', $uid)->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function storeTicket($data)
    {
        return \DB::table('tickets')->insert($data);
    }

}
