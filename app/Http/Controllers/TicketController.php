<?php

namespace App\Http\Controllers;

use App\Classes\TicketApi;
use App\Model\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    /**
     * TicketController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function getUserTickets(Request $request)
    {
        $data = $request->all();

        // return json
        return TicketApi::getTickets(auth()->id(), $data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $data = $request->all();

        Validator::make($data, [
            'event_name' => ['required', 'string'],
            'event_date' => ['required', 'string'],
            'section' => ['required', 'string'],
            'row' => ['required'],
            'seat' => ['required'],
            'status' => ['required'],
        ])->validate();

        unset($data['_token']);
        $data['user_id'] = auth()->id();

        $msg = TicketApi::storeTicket($data) ? 'true' : 'false';

        return redirect()->back()->with('status', $msg);
    }
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeCsv(Request $request)
    {

        $tickets = $request->all();
        $results = [];
        $status = 0;

        try{
            foreach ($tickets as & $data) {
                switch ($data[6]) {
                    case 'Active':
                        $status = 1;
                        break;
                    case 'Cancelled':
                        $status = 2;
                        break;
                    case 'Wasted':
                        $status = 3;
                        break;
                    case 'Sold':
                        $status = 4;
                        break;
                }

                $ticket = new Ticket();
                $ticket->user_id = auth()->id();
                $ticket->event_name = $data[1];
                $ticket->event_date = $data[2];
                $ticket->section = $data[3];
                $ticket->row = $data[4];
                $ticket->seat = $data[5];
                $ticket->status = $status;

                $ticket->save();
                $results['data'][] = $ticket;
            }
        } catch (\Exception $e){

            redirect()->back()->with('status', $e->getMessage());
        }

        $results['data'] = json_encode($results['data']);

        return response($results, 200);
    }
}
