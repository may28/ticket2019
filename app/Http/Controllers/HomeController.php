<?php

namespace App\Http\Controllers;

use App\Classes\TicketApi;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tickets = json_decode(TicketApi::getTickets(auth()->id(), []));

        return view('my-ticket', compact('tickets'));
    }

}
