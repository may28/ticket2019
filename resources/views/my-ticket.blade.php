@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 list-ticket">
                <div>
                    <h1>My Ticket <i class="fas fa-ticket-alt"></i></h1>
                </div>
                <div class="ibox ">
                    {{--filter--}}
                    <div class="ibox-title ibox-title-filter">
                        <h2>Search Tickets</h2>
                        <div class="form-inline">
                            <div class="input-group" id="search-form">
                                <div class="input-group-btn">
                                    <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">
                                        @{{ searchTypeText }}
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li :class="{ active: searchType === 1 }">
                                            <a href="#" @click="searchTypeText = 'Event Name'; searchType = 1">Event Name</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li :class="{ active: searchType === 2 }">
                                            <a href="#" @click="searchTypeText = 'Section / Row / Seat'; searchType = 2">Section / Row / Seat</a>
                                        </li>
                                    </ul>
                                </div>
                                <input type="text" class="form-control" id="searchKeyword" name="searchKeyword" v-model="searchKeyword" />
                                {{-- submit button--}}
                                <div class="input-group-btn btn-search-wrap">
                                <button class="btn btn-primary" id="btn-search-filter" @click="searcTicket()" :disabled="btnDisable">
                                    <i class="fa fa-search"></i>&nbsp;@{{ btnText }}
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--/filter --}}
                    {{-- upload csv file--}}
                    <div class="ibox-tools">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-primary btn-file"><input type="file" name="...">
                                <span class="fileinput-new btn-addfile">+ Upload CSV File</span>
                                <span class="fileinput-exists btn-addfile-change">Change</span></span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                        </div>
                    </div>
                    {{--  data list table --}}
                    <div class="ibox-content">
                        {{-- loading --}}
                        <div class="sk-spinner sk-spinner-three-bounce">
                            <div class="sk-bounce1"></div>
                            <div class="sk-bounce2"></div>
                            <div class="sk-bounce3"></div>
                        </div>
                        {{-- /loading --}}
                        <div class="table-responsive">
                            <table id="ticket-table" class="table table-hover animated fadeIn" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Ticket ID</th>
                                    <th>Event Name</th>
                                    <th>Event Date</th>
                                    <th>Section</th>
                                    <th>Row</th>
                                    <th>Seat</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="ticket in tickets">
                                    <td class="center">@{{ticket.id}}</td>
                                    <td class="center">@{{ticket.event_name}}</td>
                                    <td class="center">@{{ticket.event_date}}</td>
                                    <td class="center">@{{ticket.section}}</td>
                                    <td class="center">@{{ticket.row}}</td>
                                    <td class="center">@{{ticket.seat}}</td>
                                    <td class="center">
                                        <span class="label label-primary" v-if="ticket.status==1">Active</span>
                                        <span class="label label-danger" v-else-if="ticket.status==2">Cancelled</span>
                                        <span class="label label-warning" v-else-if="ticket.status==3">Wasted</span>
                                        <span class="label label-default" v-else>Sold</span>
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('styles')
    <link rel="stylesheet" href="{{mix('css/ticket.min.css')}}">
@endsection
@section('scripts')
    <script src="{{mix('js/ticket.min.js')}}" defer></script>
@endsection
