<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Usitour') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body class="top-navigation">
<div id="wrapper">
    <div id="app" v-cloak>
        <div id="page-wrapper" class="gray-bg">
            @guest
            @else
                <div class="row top-nav-bg">
                    {{-- top nav --}}
                    <nav class="container navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        {{-- logo --}}
                        <a href="#" class="navbar-brand">Ticket+</a>
                        {{-- right menu --}}
                        <ul class="nav navbar-top-links navbar-right">
                            {{-- user menu --}}
                            <li class="dropdown user-info">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-user"></i> {{ Auth::user()->first_name }}  {{ Auth::user()->last_name }}
                                </a>
                                <ul class="dropdown-menu dropdown-alerts animated fast fadeIn">
                                    <li>
                                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </nav>

                </div>
            @endguest

            <main class="py-4 animated fadeIn">
                @yield('content')
            </main>
        </div>
    </div>
</div>
<!-- Scripts -->
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
{{-- single page Scripts --}}
@yield('scripts')
</body>
</html>
