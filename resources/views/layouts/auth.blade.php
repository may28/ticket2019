<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    {{--<!-- CSRF Token -->--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ticket') }}</title>

    <script src="{{ mix('js/vendor.js') }}" defer></script>

    {{--<!-- Styles -->--}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    {{-- single page styles --}}
    @yield('styles')
    <style>
        main{
            align-items: center;
            display: flex;
            justify-content: center;
            height: 100vh;
        }
        #auth .container{
            margin: 0;
        }
    </style>
</head>
<body class="top-navigation">
<div id="wrapper" class="animated fadeIn faster">
    <div id="page-wrapper" class="gray-bg">
        <main id="auth">
            @yield('content')
        </main>
    </div>
</div>
</body>
</html>
