@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="loginColumns animated fadeInDown">

            <div class="col-md-6 offset-md-3 text-center title">Welcome back!</div>
            <div class="col-md-6 offset-md-3 ibox">
                <div class="ibox-content">
                    <h1>Sign in to Ticket+</h1>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        {{-- email --}}
                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- password --}}
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b" id="btn-login" onclick="submit">Login</button>

                        @if (Route::has('password.request'))
                            <a class="text-forget" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                        <p class="text-muted text-center text-account">
                            New to Ticket+ ?
                        </p>
                        <a class="btn btn-sm btn-white btn-block btn-inactive" href="/register">Create an account</a>
                    </form>
                </div>
            </div>

    </div>
</div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/login.css')}}">
@endsection
