@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="RegColumns animated fadeIn">
        <div class="col-md-6 offset-md-3 text-center title">Welcome to Ticket+ !</div>

        <div class="col-md-8 offset-md-2 ibox">
            <div class="ibox-content row position-relative">
                <div class="col-md-6 form">
                    <h1>Sign up for Ticket+</h1>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        {{--first name--}}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old
                            ('first_name') }}" placeholder="First name" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{--last name--}}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old
                            ('last_name') }}" required placeholder="Last name">

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- email --}}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                       required placeholder="E-mail Address">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- password --}}
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required
                                       placeholder="Password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b" id="btn-reg">Sign Up</button>

                        <p class="text-muted text-center text-account">Have a Ticket+ account ?</p>
                        <a class="btn btn-sm btn-white btn-block btn-inactive" href="/login">Sign In</a>
                    </form>
                </div>
                <div class="col-md-6 bg-img d-none d-sm-block">
                    <div class="Buy-and-sell-tickets">
                        Buy and sell tickets on the world’s largest ticket marketplace.
                    </div>
                    <div class="Ticket">
                        Ticket+
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/login.css')}}">
@endsection
