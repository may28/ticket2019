// const toastr = require('toas tr');
const app = new Vue({
    el: '#app',
    data: {
        tickets: [],
        searchTypeText: 'Event Name',
        searchType: 1,
        btnDisable: false,
        btnText: 'Search',
        searchKeyword: ''
    },
    mounted: function () {
        const self = this;
        //add loading before fetch data
        $('.ibox-content').addClass('sk-loading');

        // get data from api
        axios.get('/api/user/tickets').then(function (res) {
            self.tickets = res.data;
            $('.ibox-content').removeClass('sk-loading');
        }).catch(function (err) {
            console.log(err);
        });

        // upload file validation
        $('.fileinput').on('change.bs.fileinput', function(e) {
            e.stopPropagation();
            let file = $(e.delegateTarget, $('.fileinput')).
                find('input[type=file]')[0].files[0];
            let fileExtension = file.name.split('.');
            fileExtension = fileExtension[fileExtension.length -
            1].toLowerCase();

            let arrayExtensions = ['csv'];
            // check file type
            if (arrayExtensions.lastIndexOf(fileExtension) === -1) {
                toastr.error('Only CSV can be uploaded', 'Error!');
                $(this).fileinput('clear');
                return;
            }
            // check file size < 5mb
            if (file['size'] >= 5242880) {
                toastr.error('Max 5 MB of file size can be' +
                    ' uploaded.', 'Error!');
                $(this).fileinput('clear');
                return;
            }

            // Parse local CSV file
            Papa.parse(file, {
                complete: function (results) {
                    $('.ibox-content').addClass('sk-loading');
                    /* import data verification
                    not empty:
                     1: "Event Name"
                     2: "Event Date"
                     3: "Section"
                     4: "Row"
                     5: "Seat"
                     */
                    let validator = true;
                    let tickets_csv = results.data;
                    for (let i = 0; i < tickets_csv.length; i++) {
                        // check first line data, whether contain table head or not
                        if (tickets_csv[0][0] === 'Ticket ID' && i === 0) {
                            // delete first line data(table head)
                            tickets_csv.shift();
                        }
                        // check each column not empty in every row
                        for (let j = 0; j < tickets_csv[i].length; j++) {
                            if (!tickets_csv[i][j] && j !== 0 && j !== 5) {
                                validator = false;
                                toastr.error('Please check CSV file in row: ' + i + ' and column: ' + j, 'Upload CSV' +
                                    ' Error!');
                                $('.ibox-content').removeClass('sk-loading');
                                break;
                            }
                        }

                        if (!validator) break;
                    }
                    // call store api
                    axios.post('/api/csv/tickets', tickets_csv).then(function(response) {
                        $('.ibox-content').removeClass('sk-loading');
                        if (response.status === 200) {
                            toastr.success('Upload CSV file successfully!');
                            let res = JSON.parse(response.data.data);
                            res.forEach(function(ticket) {
                                self.tickets.push(ticket);
                            });

                        } else {
                            toastr.error('Add CSV file failed!');
                        }
                    }).catch(function(error) {
                        console.log(error);
                    });

                },
            });

            return false;
        });

    },
    watch: {
        tickets: function() {
            // destroy datatable before rerender the table dom
            $('#ticket-table').DataTable().destroy();
            this.$nextTick(function() {
                $('#ticket-table').DataTable({
                    'destroy': true,
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'responsive': true,
                    // change arrow rather than word
                    'language': {
                        paginate: {
                            next: '>',
                            previous: '<',
                        },
                        'search': 'Filter records:',
                        searchPlaceholder: 'Any keyword...',
                    },
                    'order': [[0, 'desc']],
                    'dom': '<"html5buttons"B>lTfgtip',
                    'buttons': [
                        {extend: 'csv', text: 'Download CSV'},
                    ],
                    'columnDefs': [
                        {responsivePriority: 1, targets: 1},
                        {responsivePriority: 2, targets: 2},
                    ],
                });
            });

        },
    },
    beforeUpdate: function (){
        $('.fileinput').fileinput('clear');
    },
    methods: {
        searcTicket: function() {
            // check keyword
            if(!this.searchKeyword){
                toastr.warning('Search keyword should not be empty');
                $('#searchKeyword').focus();

                return;
            }
            // add loading
            $('.ibox-content').addClass('sk-loading');
            this.btnText = 'Searching...';
            this.btnDisable = true;
            let self = this;
            // Make a request for tickets data with type and keyword
            axios.get('/api/user/tickets?type=' + this.searchType + '&keyword=' + this.searchKeyword)
            .then(function (response) {
                // handle success
                self.tickets = response.data;
                $('.ibox-content').removeClass('sk-loading');
                self.btnText = 'Search';
                self.btnDisable = false;

            })
            .catch(function (error) {
                // handle error
                toastr.error('error!')
            });

        },
    },
});
