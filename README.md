# README #

This is an UI develop homework task project.   
__Online Demo__: [ticket.imay.us](http://ticket.imay.us) ( Godaddy Shared Host )  
````
username: ashucn@gmail.com
password: 88888888  
```` 
  
----   
  
## A. Business Requirement  

You work for a successful ticket marketplace company. The company has popular web, iOS and Android applications for consumers.
The business is asking the team to build an app (proof of concept), and you are the UI lead developer.

##### Task:
Build a proof of concept of system described below.

### A1. Account and user setup  
 - There’s a login page in the app where user can login with their credential  
 

### A2. Ticket preview 
- User is using this app to manage his tickets. Therefore, after user login to the system, there’s a ticket view page.
- Tickets info – __Ticket ID/Event Name/Event Date/Section Info/Row Info/Seat Info__
- Suppose the user has lots of tickets, so please provide a basic filter function for user to easily search his tickets.
- Search condition – __event name or section/row/seat__ info.
- On the ticket preview page, there’s a button called “upload files”, user can use it to upload CSV files. CSV file has the ticket info.
- Think about some __validation__ during file upload.
- Tickets has status – __active/cancelled/wasted/sold__ – create a general report to show ticket status
                     
----  
          
## B. Work process

#### B1.UI Design:  
 - Using Sketch App [Download .sketch UI file](https://drive.google.com/file/d/1vFhqnd0OTR1YalWYL2CrMSThuziJmbHN/view?usp=sharing)
 - upload UI files to [Figma.com](https://www.figma.com/file/KvGGis4NXTJFLkWbNyk76q4h/my-ticket-may-201904?node-id=0%3A1)
 
#### B2.Develop (Full stack):  
 - __Front-End framework__: Bootstrap 4 + VueJS 2    
   
 Project FE structure  
   
````  
 /resoures/
 
 ├── js/                    // Javascript src
     ├── componets/         // Vue component
     ├── app.js             // Application config for js
     ├── ticket.js          // single page js file
 ├── sass/                  // sass src
     ├── app.scss           // Application config for scss
     ├── login.scss         // single page scss file
 ├── vendor/                // plugins
 ├── view/                  // Application Views page (MVC)
   
````    
  
 - __Back-End framework__: Laravel 5.8 (API)  
   
 Project BE structure  
   
````  
├── app/
    ├── Classes/                // Api Classes
    ├── Http/
        ├── Controllers/        // Application Controller classes (MVC)
            ├── HomeController.php
            ├── TicketController.php
    ├── Model/                  // Model -> Eloquent ORM 
        ├── Ticket.php          
        ├── User.php       
├── routes/                     // Application Routes
    ├── web.php/ 
      
````  
  
 - __Database__: SQLite
 - __Repository__: [Bitbucket: ticket2019](https://bitbucket.org/mayusa/ticket2019/src/5038373a66cd9136305a3b09309d672189c0d460?at=development)  
 - __Other Tech & Tool__: Zeplin.io, git, Webpack, npm, scss, Trello, SourceTree, Naivcat

#### B3.Test:  
3.1 __Online Demo__: [ticket.imay.us](http://ticket.imay.us) ( Godaddy Shared Host )  

3.2 __Local host__ testing progress
````
$ git clone https://mayusa@bitbucket.org/mayusa/ticket2019.git

// copy env file and change db config from mysql to sqlite
$ cp .env-example .env

$ touch database/database.sqlite

$ composer install

$ npm install

$ php artisan migrate

// seeding the database with test data
$ composer update fzaninotto/faker
$ php artisan db:seed
$ php artisan key:generate
$ composer update // update all component

// run local server
$ php artisan serve --port 8009

````

local host ready to go~!  
----  



* MacOS: Safari, Chrome, Firefox
* Windows: Firefox, IE>10
* iOS, Android

##### 4.Deploy:  
* Godaddy shared host: [ticket.imay.us](http://ticket.imay.us)   


----

##### 5.Dev and Deploy workflow:   

![Dev and Deploy workflow](http://ticket.imay.us/images/dev-deploy-workflow.png)
  

----
  
##### 6.Screenshot:   
![Dashboard](http://ticket.imay.us/images/dashboard.png)  

----

\# __201904 ashucn@gmail.com__
